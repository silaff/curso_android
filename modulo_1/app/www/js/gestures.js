var app = {
	inicio: function () {
		console.log("inicio");
		this.iniciaBotones();
		this.iniciaFastClick();
		this.iniciaHammer();
	},

	iniciaFastClick: function () {
		FastClick.attach(document.body);
	},

	iniciaBotones: function () {
		console.log("iniciaBotones");
		var botonClaro = document.querySelector('#claro');
		var botonOscuro = document.querySelector('#oscuro');

		botonClaro.addEventListener('click', this.ponloClaro, false);
		botonOscuro.addEventListener('click', this.ponloOscuro, false);
	},

	iniciaHammer: function () {
		console.log("iniciaHammer");
		var zona = document.getElementById('zona-gestos');
		var hammertime = new Hammer(zona);

		hammertime.get('pinch').set({ enable: true });
		hammertime.get('rotate').set({ enable: true });

		zona.addEventListener('webkitAnimationEnd', function (e) {
			zona.className = '';
		});

		/*
		hammertime.on('tap doubletap pan swipe press pinch rotate', function(ev) { 
			document.querySelector('#info').innerHTML= ev.type+'!';
		});
		*/
		hammertime.on('doubletap', function (ev) {
			zona.className = 'doubletap';
		});
		
		hammertime.on('press', function (ev) {
			zona.className = 'press';
		});

		hammertime.on('swipe', function (ev) {
			var clase = undefined;
			direccion = ev.direction;
			if ( direccion == 4 ) clase ='swipe-derecha';
			if ( direccion == 2 ) clase ='swipe-izquierda';
			zona.className = clase;
		});

		hammertime.on('rotate', function (ev) {
			console.log("rotate");
			var umbral = 25;
			if ( ev.distance > umbral ) 
				zona.className = 'rotate';
		});


	},

	ponloClaro: function () {
		console.log("ponloClaro");
		document.body.className = 'claro';
	},

	ponloOscuro: function () {
		console.log("ponloOscuro");
		document.body.className = 'oscuro';
	},

};

if ('addEventListener' in document) {
	console.log("addEventListener");
	document.addEventListener('DOMContentLoaded', function () {
		FastClick.attach(document.body);
		app.inicio();
	}, false);
}






